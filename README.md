# multitool

A container loaded up with a bunch of devops tools, persistant gcloud auth, and host-mounted .kube contexts.

## Prerequisites
1. Rancher Desktop (or Docker Desktop) must be installed and running.

## Initialize
The first time you use this tool, you must run a couple initializtion steps.

1. Change directory to this project's root
1. Run the following command to create a local .kube directory (if it does not exist):
    ```bash
    mkdir -p $HOME/.kube
    ```
1. Run the following command to build the tools
    ```bash
    docker-compose run --build tools
    ```
    1. You will need to re-run this command when you make changes to the `Dockerfile`, or want to update your local container to the latest tools versions.
1. You will need to run `gcloud auth login`, and any other commands to set up default projects and/or cluster contexts.
    1. NOTE: Your gcloud login info will persist in a local volume and will automatically be available each time you start this tool up
1. Your kubectl contexts are mounted from your local `$HOME/.kube/config`

## Running the container
1. Change directory to this project's root.
1. Update the `.env` file, setting the value of `PROJECT_ROOT` to the project you want to work on (if any).
1. Run the following command to start the tools:
    ```bash
    docker-compose run --rm tools
    ```
1. Have fun 🎉